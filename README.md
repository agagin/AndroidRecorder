# AndroidRecorder

A quick and dirty android-based Audio Recorder using the Media Recorder and Media Player libraries.
Important files: 

- app/src/main/java/com/example/recorder/MainActivity.java
- app/src/main/AndroidManifest.xml
- app/src/main/res/layout/activity_main.xml

See article.md for the assignment.
