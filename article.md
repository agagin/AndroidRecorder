# On Privacy Concerns of Android Audio Recorder Apps

Most android devices have a sound recorder app pre-installed. You can use them for recording meetings, lectures, band practices, family memories — anything you want to save and listen to later.
Here is an example of the Recorder app that comes pre-installed on Huawei phones:

![huawei1](imgs/huawei1.png "Huawei Recorder")
![huawei2](imgs/huawei2.png "Huawei Recorder")

_figure 1: Standard Huawei Recorder_

The following article will first discuss the privacy requirements and threats of recorder apps. Then we will go over a code demo using the Media Recorder library, and finish off with an analysis of privacy controls and threat mitigation. 

## Privacy Requirements and Threats
A recorder requires one permission from the user: the right to record audio (`android.permission.RECORD_AUDIO`). It will save files into its internal storage. However, if you want those files to be [shareable](https://developer.android.com/training/data-storage), then you would need to save to external storage, in which case the right to access the device's storage (`android.permission.WRITE_EXTERNAL_STORAGE`) is also needed. This write permission includes the read permission. Both of these are dangerous permissions that must be specified in the `AndroidManifest.xml` file 

In terms of threats, there are two main scenarios to consider: the content of the recordings and the use of the above two permissions outside of the app's intended use. 
The content of a recording could range from something innocuous like a shopping list to PII *(personally identifiable information)* such as addresses and passwords. On the other hand, the permissions could be exploited to record audio when the app is not supposed to, or to access private files outside of the app's file directory. 

### NIST (National Institute of Standards and Technology) threat model
Going by the [NISTIR 8062](https://nvlpubs.nist.gov/nistpubs/ir/2017/NIST.IR.8062.pdf), the possible problematic data actions that could occur during the use of recorder apps are:
 - _Surveillance_: monitoring of audio that is disproportionate to the purpose of the recorder app.
 - _Insecurity_: Lapses in data security via the access of the file system.
 - _Appropriation_: Use of PII in ways that exceed an individual’s expectation

    which can lead to the following privacy harms:

 - _Loss of self-determination_: The loss of an individual’s personal sovereignty due to being monitored / recorded without their knowledge.
 - _Loss of trust_ in recorder apps and Android
 - _Discrimination_ if the data released is sensitive


### Real life examples:
In the year 2020 alone, I found two Android recorder vulnerabilities mentioned in the National Vulnerability Database (NVD).

 -  [CVE-2020-0061](https://nvd.nist.gov/vuln/detail/CVE-2020-0061): In Pixel Recorder, there was a possible permissions bypass allowing arbitrary apps to record audio. User interaction was not needed for exploitation 
 - [CVE-2020-0228](https://nvd.nist.gov/vuln/detail/CVE-2020-0228#vulnCurrentDescriptionTitle): There was an improper configuration of recorder related service. Even though this vulnerability had a high base score (7.5), there is surprisingly little info provided. On the [Android Bulletin](https://source.android.com/security/bulletin/2020-07-01#mediaTek-components), it is classified as an ID type vulnerability of a Media Tek component, yet the exact issue is not publicly available.



  
## Code Demo
 This is the app I've made for the code demo. There are four basic buttons with the following functionalities: start recording, stop recording, play last recording, stop playing and reset recorder. 

![basic layout](imgs/Screenshot_2021-02-19_173859.png "App basic layout")
![diagram](imgs/statediagram2.png "state diagram")

_figure 2: Android Recorder Demo App and its state diagram_

To ensure proper flow, some of the buttons are set to be enabled / disabled in accordance to this state diagram. For example, at the very beginning, only the record button is clickable.


 ***IMPORTANT NOTE: The Android Emulator cannot record audio. Be sure to test your code on a real device that can record.*** 
 
### The APIs:
[Media Recorder](https://developer.android.com/reference/android/media/MediaRecorder) allows you to record audio and video. In our case, we will only need audio. This is the blueprint for creating and running such an instance.
```
MediaRecorder recorder = new MediaRecorder();
 recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
 recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
 recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
 recorder.setOutputFile(PATH_NAME);
 recorder.prepare();   //Initialize recorder
 recorder.start();   // Recording is now started
 ...
 recorder.stop();     //Recording is now stopped
 recorder.reset();   // You can reuse the object by going back to setAudioSource() step
 recorder.release(); // Now the object cannot be reused
 ```
[Media Player](https://developer.android.com/guide/topics/media/mediaplayer) has very similar functions. You just need to make sure you use the same path name as the recorder did.
```
MediaPlayer player = new MediaPlayer();
player.setDataSource(PATH_NAME);
player.prepare();
player.start();
 ...
player.stop();
player.release();
```

_Side note: Apart from Media Recorder, there are actually [two more ways](https://www.androidcookbook.info/android-media/raw-audio-recording-with-audiorecord.html) to record audio. You can use an intent to launch the default sound recorder or the Audio Record API that allows access to the raw audio stream_

### Permissions:
I've decided to write the files to external storage, as is standard with preinstalled recorders (like the Huawei one). This app asks for permissions any time you click the record button, instead of when you first open the app. This comes in handy in case the permissions are ever revoked after being granted, as we would not want the recorder to work in that case.

![storage_perm](imgs/storage.png "Storage permission")
![record_perm](imgs/record2.png "Record permission")

_figure 3: App asking for Permissions_

Let's say that we did not give record permission from the start. Then, when you click on the record button, you would be repeatedly asked for the missing permission until it is granted.

![record_perm](imgs/record.png "Record permission repeated")

_figure 4: Asking for record permission again_

This is done by putting a permission check into the event listener for that specific button. If checkPermission() passes, then we can start recording. Otherwise, we request permission.

```
  buttonStart.setOnClickListener(view -> {
            if(checkPermission()) {
               ...
               mediaRecorder=new MediaRecorder();
               ...
               mediaRecorder.start();  
            } else {
                requestPermission();
            }
```

### What happens if permissions are not acquired?
What happens if we do not check permission before starting to record audio? So in the above scenario, we would replace the condition in the `if` statement to always be true.
```
  buttonStart.setOnClickListener(view -> {
            if(1==1) {
               ...
               mediaRecorder=new MediaRecorder();
               ...
               mediaRecorder.start();  
            } else {
                requestPermission();
            }
```
In this case, the app just crashes. The reason is the following exception: ` java.lang.RuntimeException: setAudioSource failed.` As there is no permission to record audio, you can not set the audio source to be the microphone.

## Privacy Controls
As discussed above, you [must specify the permissions needed](https://developer.android.com/guide/topics/manifest/manifest-intro) in the `AndroidManifest.xml` file. This way, the use of the following permissions will be relayed to to the Android build tools, the Android operating system, and Google Play.

### On implementing permissions checking
- toast notifications are always a great idea, to let the user know if they can / can not use the app.
```
@Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0) {
                boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (StoragePermission && RecordPermission) {
                    Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
 ```
 - It is useful to have a checker function to be used in a loop like above:
 ```
    public boolean checkPermission() {
        int result_s = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result_r = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result_s == PackageManager.PERMISSION_GRANTED &&
                result_r == PackageManager.PERMISSION_GRANTED;
    }
 ```

### On privacy threats from other apps 
Remember CVE-2020-0228, the Pixel recorder vulnerability that allowed arbitrary apps to record audio? This is type of attack is known as a covert channel, which can enable communication between two colluding apps so that one app can share its permission protected data with another app lacking those permissions. The implications of these channels have been discussed in the following study: [50 Ways to Leak Your Data: An Exploration of Apps’ Circumvention of the Android Permissions System](https://www.ftc.gov/system/files/documents/public_events/1415032/privacycon2019_serge_egelman.pdf)

![cc](imgs/covertchannel.png "Covert Channel Diagram")

_figure 5: A security mechanism
allows app1 access to resources but denies app2 access; this is
circumvented by app2 using app1 as a facade to obtain access
over a communication channel not monitored by the security
mechanism_

This scenario happens if apps use common SDK (software development kit) libraries embedded within the app. To the average user, it is impossible to know if an app uses any such SDK. The study authors relayed these issues to Google, who responded by saying that they would fix them in their Android Q release. However, privacy shouldn’t be treated like a luxury good, as only those with the money to buy a newer device capable of running Android 10 and above will be protected. For example, my own phone is running on Android 9 yet it is only three years old.

### On raising user awareness
The user can be made aware of the Recorder running by using [notifications](https://developer.android.com/training/notify-user/build-notification). Just by adding a simple icon to the status bar, we can make sure that the users know what is running on their phone right now. However, if multiple apps have status icons as well, the user might get annoyed.

![notif](imgs/notif.png "Status Bar")

_figure 6: Status Bar_

This is quite important, as a recorder can be running without the user realizing it! For example, for my demo app, once I click record, it will keep recording if the screen goes black. It will also keep recording if I switch to a different app. It will only stop if the app is terminated or if the finish recording button is pressed.

### Side story: Is My Phone Listening in?
While we are on the topic of recording audio when it is not supposed to be recorded; there is quite a controversial question floating around: is my phone listening to me? You are probably familiar with the story of somebody speaking with their friend about AirPods and getting ads about them the very next day. Outside of multiple [news articles](https://www.usatoday.com/story/tech/columnist/2019/12/19/your-smartphone-mobile-device-may-recording-everything-you-say/4403829002/), there are also studies such as this one: [Is My Phone Listening in? On the Feasibility and Detectability of Mobile Eavesdropping](https://link.springer.com/chapter/10.1007/978-3-030-22479-0_6#Sec3)

![eavesdrop](imgs/eavesdrop.png "A schematic and simplified overview of the threat model.")

_figure 7: A schematic and simplified overview of the eavesdropping threat model._

For this specific ad scenario, an app would require the permission to access the internet.
Another thing to consider is that the recording must be happening as a background service. For that, one would typically need the wave-lock permission, which is a mechanism to indicate that your application needs to have the device stay on. Keep in mind, permissions aren't faultless, as by using a covert channel, nothing is stopping a separate app with access to internet from accessing the files of a seemingly safe recorder app.

Outher than looking at permissions, a user can check what background services they have running on their phone by going `Settings > System > Developer Options > Running services`. Here you can view which processes are running, your used and available RAM, and which apps are using it up. There is a catch: normal users do not have "Developer Options" enabled.

Thus, the spying fears have not been disproved so far, neither by device manufacturers and ecosystem providers nor by the research community.

## References

 - NVD: CVE-2020-0228 [https://nvd.nist.gov/vuln/detail/CVE-2020-0228#vulnCurrentDescriptionTitle](https://nvd.nist.gov/vuln/detail/CVE-2020-0228#vulnCurrentDescriptionTitle)
 - NVD: CVE-2020-0061 [https://nvd.nist.gov/vuln/detail/CVE-2020-0061](https://nvd.nist.gov/vuln/detail/CVE-2020-0061)
 - Android Bulletin for CVE-2020-0228[https://source.android.com/security/bulletin/2020-07-01#mediaTek-components](https://source.android.com/security/bulletin/2020-07-01#mediaTek-components) 
 - NISTIR 8062 [https://nvlpubs.nist.gov/nistpubs/ir/2017/NIST.IR.8062.pdf](https://nvlpubs.nist.gov/nistpubs/ir/2017/NIST.IR.8062.pdf)
 - Is My Phone Listening in? On the Feasibility and Detectability of Mobile Eavesdropping[https://link.springer.com/chapter/10.1007/978-3-030-22479-0_6#Sec3](https://link.springer.com/chapter/10.1007/978-3-030-22479-0_6#Sec3)
 - You’re not paranoid: Your phone really is listening in [https://www.usatoday.com/story/tech/columnist/2019/12/19/your-smartphone-mobile-device-may-recording-everything-you-say/4403829002/](https://www.usatoday.com/story/tech/columnist/2019/12/19/your-smartphone-mobile-device-may-recording-everything-you-say/4403829002/)
 - Raw Audio Recording with Audio Record [https://www.androidcookbook.info/android-media/raw-audio-recording-with-audiorecord.html](https://www.androidcookbook.info/android-media/raw-audio-recording-with-audiorecord.html)
 - Android Data Storage [https://developer.android.com/training/data-storage](https://developer.android.com/training/data-storage)
 - Android Media Recorder [https://developer.android.com/reference/android/media/MediaRecorder](https://developer.android.com/reference/android/media/MediaRecorder)
 - Android Media Player [https://developer.android.com/guide/topics/media/mediaplayer](https://developer.android.com/guide/topics/media/mediaplayer)
 - Android Notifications [https://developer.android.com/training/notify-user/build-notification](https://developer.android.com/training/notify-user/build-notification)
 - Android Wavelock [https://developer.android.com/reference/android/os/PowerManager.WakeLock](https://developer.android.com/reference/android/os/PowerManager.WakeLock)
 - Android Manifest [https://developer.android.com/guide/topics/manifest/manifest-intro](https://developer.android.com/guide/topics/manifest/manifest-intro)
